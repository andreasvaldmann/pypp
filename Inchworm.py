#!/usr/bin/env python2
# coding=utf8

# v2012-01-31 16:03 Peeter Piksarv
# vlabor - default port is /dev/ttyS2

# Class for using Inchworm translator

import serial
import time
import numpy as np

class inchworm:
    timeout = 0.05 # The default timeout to wait for an answer from device in seconds
    
    def __init__(self, port = '/dev/ttyS2'):
        self.serial = serial.Serial(port = port, baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_TWO, bytesize=serial.EIGHTBITS)
        #self.serial.open()
        
    def isOpen(self):
        """ Check if the port is opened """
        return self.serial.isOpen()
        
    def write(self, data, timeout = timeout):
        """ Output the given string over the serial port and wait a given timeout for a device to respond """
        self.serial.write(data)
        time.sleep(timeout)
        
    def clearBuffer(self):
        """ Clear read buffer """
        while self.serial.inWaiting() > 0:  # Clear imput buffer
            self.serial.read(1)
            
    def wait(self):
        """ Wait for output for commands that should give one """
        n = self.serial.inWaiting()
        while self.serial.inWaiting() < n+14:
            time.sleep(0.005)
        self.clearBuffer()
        
    def getPos(self):
        """ Get current position """
        self.write('1')
        return self.readInfo()
                
    def readInfo(self):
        """ Read the last output from device """
        out = ''
        if self.serial.inWaiting() > 0:
            while self.serial.inWaiting() > 0:
                out += self.serial.read(1)
            out = out.splitlines()[-1][-9:]
            if out.find(' ')>-1:
                out = out[-5:]
            return int(out)       
    
    def setHome(self):
        """ Set current position to 0 """
        self.write('Z')    
        
    def goHome(self):
        """ Move to position 0 """
        self.write('G')
        self.wait()
        
    
    def getMode(self):
        """ Get info of the current operation mode """
        self.write('1')
        out = ''
        while self.serial.inWaiting() > 0:
            out += self.serial.read(1)
        if out[2] == 'R':
            return 'R'
        elif out[2] == 'S':
            return 'S'
            
    def setMode(self, mode = 'S'):
        """ Put the device in step 'S' or run 'R' mode """
        if 'SR'.find(mode) > -1:
            self.write(mode)
        
    def getTravel(self):
        """ Get current travel distance """
        self.write('T')  # Put here 'D' for backward travel
        travel = self.readInfo()
        self.write('\r')
        return travel
        
    def setTravel(self, travel=2):
        """ Set travel distance """
        if travel < 1:
            return -1
        self.write('T')  # Put here 'D' for backward travel
        travel = str(travel)
        for i in travel:
            self.write(i, timeout = 0.01)
        self.write('\r')
        
    def Stop(self):
        """ Stop """
        self.write('H')
        
    def goFwd(self):
        """ Go forward """
        self.write('F')
        
    def goRev(self):
        """ Go backward """
        self.write('B')
        
    def goStepFwd(self, travel=2):
        """ Step forward given distance """
        self.setMode('S')
        self.setTravel(travel)
        self.goFwd()
        self.wait()
        
    def goStepRev(self, travel=2):
        """ Step backward given distance """
        self.setMode('S')
        self.setTravel(int(travel))
        self.goRev()
        self.wait()
        
    def goRunFwd(self):
        """ Run forward until stop is given or error occurs """
        self.setMode('R')
        self.goFwd()
        
    def goRunRev(self):
        """ Run backward until stop is given or error occurs """
        self.setMode('R')
        self.goRev()
        
    def getSpeed(self):
        """ Get current forward travel speed """
        self.write('Q')  # Use 'A' for backward travel speed
        speed = self.readInfo()
        self.write('\r')
        return speed
        
    def setSpeed(self, speed = 900):
        """ Set forward travel speed 1 - 1000 """
        self.write('Q')
        if 0 < speed <= 1000:
            speed = str(int(speed))
            for i in speed:
                self.write(i, timeout = 0.01)
        self.write('\r')
        
    def goTo(self, pos = 0):
        cpos = self.getPos()
        travel = pos - cpos
        if np.sign(travel) < 0:
            self.goStepRev(np.abs(travel))
        elif np.sign(travel) > 0:
            self.goStepFwd(travel)
        