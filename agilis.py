#!/usr/bin/env python

# v2012-05-04 12:37 Peeter Piksarv
# vlabor - default port is /dev/ttyS2

# Class for using Newport Motion controller ESP301
# All units mm!

import serial
import time
import numpy as np

# TODO add serial numbers of the devices
# TODO add exact movement ranges
# TODO add calibration warnings


class Serial(serial.Serial):

    def __init__(self, port='COM4'):
        super(Serial, self).__init__(port=port,
                                     baudrate=921600,
                                     parity=serial.PARITY_NONE,
                                     stopbits=serial.STOPBITS_ONE,
                                     bytesize=serial.EIGHTBITS,
                                     )


class AgilisStage:

    # Serial numbers and corresponding movent ranges in mm
    movement_ranges = {'7920': 26.7,
                       '9081': 12.,
                       '9101': 12.}
    
    def __init__( self, device_ID, port='COM4', axis='1', channel='1', ser=None):
        """
        Initialize Newport Agilis connected actuator.
            device_ID -- 4-digit serial number of the stage (last 4 digits of 'S/N B17 XXXX')
            axis -- Axis number '1', '2'
            channel -- Channel number '1', '2', '3', '4'
        """
        if axis not in ['1', '2']:
            axis = '1'

        if channel not in ['1', '2', '3', '4']:
            channel = '1'

        self.device_ID = device_ID
        self.axis = str(axis)
        self.channel = str(channel)
        if ser is None:
            self.serial = serial.Serial(port=port,
                                        baudrate=921600,
                                        parity=serial.PARITY_NONE,
                                        stopbits=serial.STOPBITS_ONE,
                                        bytesize=serial.EIGHTBITS,
                                        )
        else:
            self.serial = ser

        # Select the movement range of the stage according to the device serial number.
        try:
            self.movement_range = AgilisStage.movement_ranges[str(self.device_ID)]
        except KeyError:
            self.movement_range = 12.0
            print('Warning: invalid device ID. Movement range set to 12 mm.')

        #self.movement_range = 12.0  # Axis movement range in mm
        self.forward_step_size = np.inf  # Step size in mm
        self.backward_step_size = np.inf  # Step size in mm
        self.current_absolute_position = 0.0  # Current position in mm

        # Set step amplitude to 50
        self.setStepAmplitude(50, '+')
        self.setStepAmplitude(50, '-')

        self.show_commands = False

    def isOpen(self):
        """ Check if the port is opened """
        return self.serial.isOpen()

    def write(self, data):
        """ Output the given string over the serial port """
        if self.show_commands:
            print("Write:", (data + '\n').encode('ascii'))
        # Select the right channel
        self.serial.write(('CC' + self.channel + '\r\n').encode('ascii'))
        self.serial.flush()
        time.sleep(0.1)
        # Send the actual command
        self.serial.write((data + '\r\n').encode('ascii'))
        self.serial.flush()
        time.sleep(0.1)

    def read(self, strip_command=''):
        """ Read the last output from device """
        out = self.serial.readline()[:-2]
        if self.serial.inWaiting() > 0:
            out = self.read()
        if self.show_commands:
            print('Read:', out)
        if len(strip_command) == 0:
            return out
        else:
            return int(out.replace(strip_command, ''))  # The return string begins with the command. Remove it.

    def clearBuffer(self):
        """ Clear read buffer """
        while self.serial.inWaiting() > 0:  # Clear input buffer
            self.serial.read(1)

    def wait(self, wait_time=1):
        """ Wait for stop """
        self.clearBuffer()

        status = -1
        while status != 0:
            status = self.getStatus()
            time.sleep(wait_time)

    def getPos(self):
        """ Get current position """
        return self.current_absolute_position

    def stop(self):
        """ Stop """
        self.write(self.axis + 'ST')

    def step(self, steps = 0):
        """Relative move by given number of steps"""
        self.write(self.axis + 'PR' + str(steps))

    def goTo(self, pos = 0.0):
        """Absolute move in mm"""
        self.goBy(pos - self.current_absolute_position)

    def goBy(self, distance = 0.0):
        """Relative move in mm"""
        if distance > 0:
            steps = int(round(distance / self.forward_step_size))
        elif distance < 0:
            steps = -int(round(distance / self.backward_step_size))
        else:
            steps = 0
        self.current_absolute_position += distance
        self.write(self.axis + 'PR' + str(steps))

    def goLim(self, jog_mode='4'):
        """Go to hardware limit.

        Jog modes:
        -4 -- Negative direction, 666 steps/s at defined step amplitude.
        -3 -- Negative direction, 1700 steps/s at max. step amplitude.
        -2 -- Negative direction, 100 step/s at max. step amplitude.
        -1 -- Negative direction, 5 steps/s at defined step amplitude.
        0 -- No move, go to READY state.
        1 -- Positive direction, 5 steps/s at defined step amplitude.
        2 -- Positive direction, 100 steps/s at max. step amplitude.
        3 -- Positive direction, 1700 steps/s at max. step amplitude.
        4 -- Positive direction, 666 steps/s at defined step amplitude.
        """
        if jog_mode not in map(lambda x: str(x), range(-4, 5)):
            jog_mode = '4'
        self.write( self.axis + 'MV' + jog_mode)

    def isInLim(self):
        """Check if the current axis is at the limit."""
        command = 'PH'
        self.write(command)
        limit_signal = self.read(strip_command=command)
        if limit_signal in [1, 3]:
            return True
        else:
            return False

    def getError( self ):
        """ Read error code"""
        self.write('TE')
        return self.read()

    def getStepAmplitude(self, dir='+'):
        """Get step amplitude in relative units.
        Direction: '+' for forward, '-' for backward"""
        if dir not in [ '+', '-' ]:
            dir = '+'
        command = self.axis + 'SU' + dir + '?'
        self.write(command)
        return self.read(strip_command=command.strip('?'))

    def setStepAmplitude(self, amplitude, dir='+'):
        """Get step amplitude in relative units.
        Direction: '+' for forward, '-' for backward"""
        if dir not in [ '+', '-' ]:
            dir = '+'
        self.write(self.axis + 'SU' + dir + str(amplitude))

    def getSteps(self):
        """Returns the number of accumulated steps in forward direction minus the number of steps
        in backward direction since powering the controller or since the last ZP (zero position) command,
        whatever was last."""
        command = self.axis + 'TP'
        self.write(command)
        return self.read(strip_command=command)

    def zeroSteps(self):
        """Reset the step counter to zero. """
        self.write(self.axis + 'ZP')

    def getStatus(self):
        """Get current motor status.

        Codes:
        0 Ready (not moving)
        1 Stepping (currently executing a PR command)
        2 Jogging (currently executing a JA command with command parameter different than 0).
        3 Moving to limit (currently executing MV, MA, PA commands)
        """

        command = self.axis + 'TS'
        self.write(command)
        return self.read(strip_command=command)

    def measureCurrentPosition(self):
        """Measure the distance of the current position in mm.
        The execution of the command can last up to 2 minutes."""
        # TODO change current absolute position?
        command = self.axis + 'MA'
        self.write(command)

        # Wait for the controller to respond
        while self.serial.inWaiting == 0:
            time.sleep(1)

        return self.read(strip_command=command)/1000.0*self.movement_range

    def calibrate(self):
        """Calibrate axis"""

        # Count the number of steps from the negative limit to the positive limit
        self.goLim('-3')  # Move to negative limit
        self.wait()
        self.zeroSteps()  # Reset step counter
        self.step(100)    # Move out of the limit
        self.wait()
        self.goLim('4')   # Move to positive limit
        self.wait()
        forward_steps = self.getSteps()  # Get number of steps travelled

        # Count the number of steps from the positive limit to the negative limit
        self.zeroSteps()  # Reset step counter
        self.step(-100)   # Move out of the limit
        self.wait()
        self.goLim('-4')   # Move to positive limit
        self.wait()
        backward_steps = self.getSteps()  # Get number of steps travelled

        self.forward_step_size = self.movement_range / forward_steps  # Set forward step size in mm
        self.backward_step_size = self.movement_range / backward_steps  # Set forward step size in mm
        self.current_absolute_position = 0.0
