#!/usr/bin/env python2
# coding=utf8

# v2012-12-08 17:19 Peeter Piksarv
# Refractive index database

import numpy as np

def refindex( wavelength=632.8, material='N-BK7' ):
    """Returns refractive index of a material.
    
    Wavelength must be given in nm.
    
    Materials currently supported:
    air 230--1690 nm [Philip E. Ciddor, Refractive index of air: new equations for the visible and near        infrared, Appl. Optics 35, 1566-1673 (1996)]
    N-BK7 300--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog]
    FS  210--3710 nm [Handbook of Optics, vol II, table 23, 1995]
    N-SF10 380--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog via http://refractiveindex.info/?group=SCHOTT&material=N-SF10]
    N-SF11 370--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog via http://refractiveindex.info/?group=SCHOTT&material=N-SF11]
    SF10 380--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog via http://refractiveindex.info/?group=SCHOTT&material=SF10]
    F2 320--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog via http://refractiveindex.info/?group=SCHOTT&material=F2]
    N-F2 365--2500 nm [SCHOTT Optical Glass Data Sheet-Catalog via http://refractiveindex.info/?group=SCHOTT&material=N-F2]
    Al2O3-o & Al2O3-e 200--5500 nm [Handbook of Optics, 3rd edition, Vol. 4. McGraw-Hill 2009 via http://refractiveindex.info/?group=CRYSTALS&material=Al2O3]
    CaF2 138--2326 nm 20C [M. Daimon and A. Masumura. High-Accuracy Measurements of the Refractive Index and its Temperature Coefficient of Calcium Fluoride in a Wide Wavelength Range from 138 to 2326 nm, Appl. Opt. 41, 5275-5281 (2002) doi:10.1364/AO.41.005275 via http://refractiveindex.info/?group=CRYSTALS&material=CaF2]
    ZnSe 550--18000 nm [Handbook of Optics, 3rd edition, Vol. 4. McGraw-Hill 2009 via http://refractiveindex.info/?group=CRYSTALS&material=ZnSe]
    """
    
    n = {
        'vac':    lambda L: 1,
        'air':    lambda L: 1 + 5792105e-8/(238.0185 - L**-2) \
                            + 167917e-8/(57.362 - L**-2),
        'N-BK7':  lambda L: np.sqrt( 1 + 1.03961212 * L**2 / ( L**2 - 0.00600069867 ) \
                                     + 0.231792344 * L**2 / ( L**2 - 0.0200179144 ) \
                                     + 1.01046945 * L**2 / ( L**2 - 103.560653 )),
        'FS' :    lambda L: np.sqrt( 1 + 0.6961663 * L**2 / ( L**2 - 0.0684043**2 ) \
                                     + 0.4079426 * L**2 / ( L**2 - 0.1162414**2 ) \
                                     + 0.8974794 * L**2 / ( L**2 - 9.896161**2 )),
        'N-SF10': lambda L: np.sqrt( 1 + 1.62153902 * L**2 / ( L**2 - 0.0122241457 ) \
                                     + 0.256287842 * L**2 / ( L - 0.0595736775 ) \
                                     + 1.64447552 * L**2 / ( L - 147.468793 )),
        'N-SF11': lambda L: np.sqrt( 1 + 1.73759695 * L**2 / ( L**2 - 0.013188707 ) \
                                     + 0.313747346 * L**2 / ( L**2 - 0.0623068142 ) \
                                     + 1.89878101 * L**2 / ( L**2 - 155.23629)),
        'SF10':   lambda L: np.sqrt( 1 + 1.61625977 * L**2 / ( L**2 - 0.0127534559 ) \
                                     + 0.259229334 * L**2 / ( L**2 - 0.0581983954 ) \
                                     + 1.07762317 * L**2 / ( L**2 - 116.60768 )),
        'N-F2':   lambda L: np.sqrt( 1 + 1.39757037 * L**2 / ( L**2 - 0.00995906143 ) \
                                     + 0.159201403 * L**2 / ( L**2 - 0.0546931752 ) \
                                     + 1.2686543 * L**2 / ( L**2 - 119.248346 )),
        'F2':     lambda L: np.sqrt( 1 + 1.34533359 * L**2 / ( L**2 - 0.00997743871 ) \
                                     + 0.209073176 * L**2 / ( L**2 - 0.0470450767 ) \
                                     + 0.937357162 * L**2 / ( L**2 - 111.886764 )),
        'Al2O3-o': lambda L: np.sqrt( 1 + 1.4313493 * L**2 / ( L**2 - 0.0726631**2 ) \
                                     + 0.65054713 * L**2 / ( L**2 - 0.1193242**2 ) \
                                     + 5.3414021 * L**2 / ( L**2 - 18.028251**2 )),
        'Al2O3-e': lambda L: np.sqrt( 1 + 1.5039759 * L**2 / ( L**2 - 0.0740288**2 ) \
                                     + 0.55069141 * L**2 / ( L**2 - 0.1216529**2 ) \
                                     + 6.5927379 * L**2 / ( L**2 - 20.072248**2 )),
        'CaF2':   lambda L: np.sqrt( 1 + 0.443749998 * L**2 / ( L**2 - 0.00178027854 ) \
                                     + 0.444930066 * L**2 / ( L**2 - 0.00788536061 ) \
                                     + 0.150133991 * L**2 / ( L**2 - 0.0124119491 ) \
                                     + 8.85319946 * L**2 / ( L**2 - 2752.28175 )),
        'ZnSe':   lambda L: np.sqrt( 1 + 4.2980149 * L**2 / ( L**2 - 0.1920630**2 ) \
                                     + 0.62776557 * L**2 / ( L**2 - 0.37878260**2 ) \
                                     + 2.8955633 * L**2 / ( L**2 - 46.994595**2 ))
    }.get( material )( wavelength*1e-3 )
    
    return n
