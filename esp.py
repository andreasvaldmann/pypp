#!/usr/bin/env python

# v2012-05-04 12:37 Peeter Piksarv
# vlabor - default port is /dev/ttyS2

# Class for using Newport Motion controller ESP301
# All units mm!

import serial
import time
import numpy as np

class Esp:
    
    def __init__( self, port = 'COM7', axis='1', home=True, ser=None ):
        """
        Intialize Esp 301 connected actuator.
            axis -- Axis number '1', '2', '3'
            home -- True/False if the current position should be regarded as home position, otherwise will find the negative hardware limit.
        """
        if axis not in ['1', '2', '3']:
            axis = '1'
            
        self.axis = str(axis)
        if ser is None:
            self.serial = serial.Serial(port=port,
                                        baudrate=19200,
                                        parity=serial.PARITY_NONE,
                                        stopbits=serial.STOPBITS_ONE,
                                        bytesize=serial.EIGHTBITS,
                                        )
        else:
            self.serial = ser
        
        #self.serial.open()
        self.motorON()                  # Motor on
        #if home:
        #    self.setHome()
        #    self.goHome('0')                   # Go to home position
        #else:
        #    self.goHome()
        self.wait()                     # Check that motion has stopped.
        
        # Read actuator info
        self.info = {}
        
        # Stage model and serial number
        self.write( self.axis + 'ID?' )
        self.info[ 'ID' ] = self.serial.readline()[:-2]
        
        # Maximum velocity
        self.write( self.axis + 'VU?' )
        self.info[ 'maxVel' ] = self.read()
        
        # Maximum acceleration
        self.write( self.axis + 'AU?' )
        self.info[ 'maxAccel' ] = self.read()
        
        self.negative_limit = -100
        self.positive_limit = 164.1
        
    def isOpen(self):
        """ Check if the port is opened """
        return self.serial.isOpen()
        
    def write(self, data):
        """ Output the given string over the serial port """
        self.serial.write( (data + '\r\n').encode('ascii') )
        
    def clearBuffer(self):
        """ Clear read buffer """
        while self.serial.inWaiting() > 0:  # Clear imput buffer
            self.serial.read(1)
            
    def motionDone( self ):
        """ Read motion done status """
        self.write( self.axis + 'MD?' )
        done = self.read()
        return bool(done)
            
    def wait(self):
        """ Wait for stop """
        self.clearBuffer()
        self.write( self.axis + 'WS' )   # Wait for stop
        self.write( self.axis + 'MD?' )  # Read motion done status
        done = self.serial.readline()[:-2]
        
    def getPos(self):
        """ Get current position """
        self.write( self.axis + 'TP' )
        return self.read()
                
    def read(self):
        """ Read the last output from device """
        out = self.serial.readline()[:-2]
        if self.serial.inWaiting() > 0:
            out = self.read()
        return float(out)
    
    def setHome(self, pos=0 ):
        """ Set current position to 0 """
        self.write( self.axis + 'DH0' + str(pos))    
        
    def goHome(self, method='' ):
        """ Move to position 0
        method is one of the following:
            '0' -- Find +0 Position Count
            '1' -- Find Home and Index Signals
            '2' -- Find Home Signals
            '3' -- Find Positive Limit Signal
            '4' -- Find Negative Limit Signal
            '5' -- Find Positive Limit and Index Signals
            '6' -- Find Negative Limit and Index Signals
        """
        if method not in [ '0', '1', '2', '3', '4', '5', '6' ]:
            method = ''
        
        self.write( self.axis + 'OR' + method )
        
    def stop(self):
        """ Stop """
        self.write( self.axis + 'ST')
        
    def goTo(self, pos = 0 ):
        if self.negative_limit < pos < self.positive_limit: 
            self.write( self.axis + 'PA' + str(pos) )
        else:
            print('Warning: position {} is out of limits [{}, {}]!'.format(pos, self.negative_limit, self.positive_limit))
    
    def goBy(self, step = 0 ):
        self.write( self.axis + 'PR' + str(step) )
        
    def getAccel( self ):
        """ Get acceleration (units/s^2) """
        self.write( self.axis + 'AC?' )
        return self.read()
        
    def setAccel( self, acc = 10. ):
        """ Set acceleration (units/s^2) """
        
        if acc <= self.info['maxAccel']:
            self.write( self.axis + 'AC' + str(acc) )
    
    def getDecel( self ):
        """ Get deceleration (units/s^2) """
        self.write( self.axis + 'AG?' )
        return self.read()
        
    def setDecel( self, acc = 10. ):
        """ Set deceleration (units/s^2) """
        
        if acc <= self.info['maxAccel']:
            self.write( self.axis + 'AG' + str(acc) )
        
    def motorStatus( self ):
        """
        Get motor status:
             1 motor power ON
             0 motor power OFF
        """
        self.write( self.axis + 'MO?' )
        return self.read()
       
    def motorON( self ):
        """ Turn motor ON """
        self.write( self.axis + 'MO' )
    
    def motorOFF( self ):
        """ Turn motor OFF """
        self.write( self.axis + 'MF' )
       
    def goLim( self, dir='+' ):
        """ Go to hardware limit: '+' for positive and '-' for negative limit. """
        if dir not in [ '+', '-' ]:
            dir = '+'
            
        self.write( self.axis + 'MT' + dir )
       
    def getError( self ):
        """ Read error message(s) """
        err = ''
        cerr = ''
       
        while cerr[:1] != '0':           # Read all errormessages until no error is returned
            err += cerr
            self.write( 'TB?' )
            cerr = self.serial.readline()
            
        return err
       
    def getVel( self ):
        """ Get velocity (units/s) """
        self.write( self.axis + 'VA?' )
        return self.read()
         
    def setVel( self, vel = 2.5 ):
        """ Set velocity (units/s) """
        
        if vel <= self.info['maxVel']:
            self.write( self.axis + 'VA' + str(vel) )
