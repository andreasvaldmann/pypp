#!/usr/bin/env python2
# coding=utf8

# v2012-12-12 17:20 Peeter Piksarv

# Class for using firewire cameras
# AVT Stingray F504B
# AVT Guppy Pro F503B

import pydc1394
import numpy
import pygame
import time

import matplotlib.pyplot
import scipy.ndimage.interpolation

import threading

from ctypes import c_uint32, byref

REG_CAMERA_AVT_TIMEBASE = 0x208
REG_CAMERA_AVT_EXTD_SHUTTER = 0x20C
REG_CAMERA_AVT_HSNRR = 0x520


class DC1394Error(Exception):
    """Base class for Exceptions"""
    pass


class CameraError(DC1394Error, RuntimeError):
    pass


class Camera:
    
    def __init__(self, n = 0):
        l = pydc1394.DC1394Library()
        cams = l.enumerate_cameras()
        
        self.cam0 = pydc1394.Camera(l, cams[n]['guid'])
        
        # Select camera mode:
        m, = filter(lambda m: m.name == "FORMAT7_0", self.cam0.modes)
        self.cam0.mode = m
        m.setup(m.max_image_size,(0,0),'Y16')
        
        self.cam0.shutter.mode = 'manual'
        self.cam0.shutter.val = 1000  #self.cam0.shutter.range (1, 4095)

        self.cam0.isospeed=800

    def getShutter(self):
        """ Get current camera shutter value """
        return self.cam0.shutter.val
        
    def setShutter(self, shutter = 1000):
        """ Set camera shutter value in range (1, 4095) """
        self.cam0.shutter.val = shutter
        
    def getTimebase( self ):
        """ Get current shutter timebase id.
        Corresponding times are:
            ID  Time base in um
            0      1
            1      2
            2      5
            3     10
            4     20
            5     50
            6    100
            7    200
            8    500
            9   1000
        """
        
        value = self.get_adv_register( REG_CAMERA_AVT_TIMEBASE )
        
        # Time base ID: bits 29..31
        return value & 0xf
    
    def setTimebase( self, timebase_id ):
        """ Set shutter timebase 0..9
        Corresponding times are:
            ID  Time base in um
            0      1
            1      2
            2      5
            3     10
            4     20
            5     50
            6    100
            7    200
            8    500
            9   1000
        """
            
        # Get current timebase
        curval = self.get_adv_register( REG_CAMERA_AVT_TIMEBASE )
        
        newval = (curval & 0xfffffff0) | (timebase_id & 0xf)
        self.set_adv_register( REG_CAMERA_AVT_TIMEBASE, newval )
        
    def getExtdShutter( self ):
        """ Get extended shutter value in us """
        
        value = self.get_adv_register( REG_CAMERA_AVT_EXTD_SHUTTER )
        
        # Exposure Time in us: Bits 6..31
        return value & 0xfffffff
    
    def setExtdShutter( self, value ):
        """ Set extended shutter value in us """
        
        # Get current value
        curval = self.get_adv_register( REG_CAMERA_AVT_EXTD_SHUTTER )
        
        newval = (curval & 0xf0000000) | (value & 0x0fffffff)
        self.set_adv_register( REG_CAMERA_AVT_EXTD_SHUTTER, newval )
        
    def getGain(self):
        """ Get current camera gain value """
        return self.cam0.gain.val
    
    def setGain(self, gain = 0.0):
        """ Set camera gain value in range (0.0, 23.517) """
        self.cam0.gain.val = gain
        
    def getHighSNR( self ):
        """ Get status of High SNR mode
        With High SNR mode enabled the camera internally grabs grabCount 
        images and outputs a single averaged image.
        
        Returns staus ON/OFF and grabCount"""
        value = self.get_adv_register( REG_CAMERA_AVT_HSNRR )
        
        # ON / OFF : Bit 6
        on_off = (value & 0x2000000) >> 25
        
        # grabCount: Bits 23..31
        grabCount = value & 0x1ff
        
        return on_off, grabCount
    
    def setHighSNR( self, on_off = 0, grabCount = 0 ):
        """ Set status of High SNR mode
        With High SNR mode enabled the camera internally grabs grabCount 
        images and outputs a single averaged image.
        
        grabCount possible values: 2, 4, 8, 16, 32, 64, 128, 256"""
        
        # Get current value
        curval = self.get_adv_register( REG_CAMERA_AVT_HSNRR )
        
        newval = (curval & 0xfdffffff) | ( on_off << 25 )
        newval = (newval & 0xfffffe00) | grabCount
        
        self.set_adv_register( REG_CAMERA_AVT_HSNRR, newval )
        
        
    def getFrame(self, dtype = numpy.float64):
        """ Grab one frame from camera """
        self.cam0.start()
        frame = self.cam0.shot()
        self.cam0.stop()
        if frame.max() >= 65520:
            print("Warning: Overexposure detected!")
        return frame.astype(dtype)
    
    def showLiveVideo(self, scale=0.5, zoom=1, show_crosshair=False ):
        """ Shows live camera video """
        LiveWindow(self, scale, zoom, show_crosshair).start()
    
    def get_register( self, offset ):
        """Get the control register value of the camera a the given offset"""
        return self.cam0.get_register( offset )
    
    def set_register( self, offset, value ):
        """Set the control register value of the camera at the given offset to
        the given value"""
        self.cam0.set_register( offset, value )
    
    def get_adv_register( self, offset ):
        """Get the advanced control register value of the camera a the given offset"""
        if not self.cam0._cam:
            raise CameraError("The camera is not opened!")
        
        val = c_uint32()
        self.cam0._dll.dc1394_get_adv_control_registers( self.cam0._cam, offset, byref(val), 1)
        return val.value
    
    def set_adv_register( self, offset, value ):
        """Set the advanced control register value of the camera at the given offset to
        the given value"""
        if not self.cam0._cam:
            raise CameraError("The camera is not opened!")
        
        val = c_uint32(value)
        self.cam0._dll.dc1394_set_adv_control_registers( self.cam0._cam, offset, byref(val), 1)

    def autoShutter(self, shutter=0, targetlevel=0.85, deviation=0.1):
        """Set camera shutter to optimal value without overexposure
        shutter -- initial shutter value
        targetlevel -- target signal level of the brightest pixel (0-1)
        deviation -- maximum allowed signal deviation from target level"""
        maxvalue = 65500.
        minshutter = 1
        maxshutter = 4095

        if shutter==0:
            shutter = self.getShutter()
        
        while True:
            shutter = round(shutter)
            self.setShutter(shutter)
            time.sleep(0.3)  # give time to change shutter value (needed in Windows)
            level = self.getFrame().max()/maxvalue

            if level >= 1.:  # overexposure
                if shutter<=minshutter:  # overexposure with minimum shutter time
                    shutter = minshutter
                    break
                else:
                    shutter = shutter/10.
            elif abs(level - targetlevel) > deviation: # exposure level out of target range
                shutter = shutter*targetlevel/level
                if shutter >= maxshutter:  # underexposure with maximum shutter time
                    shutter = maxshutter
                    self.setShutter(shutter)
                    break
            else:  # exposure level in target range
                break
            
        return int(shutter)


class LiveWindow ( threading.Thread ):
    def __init__ ( self, cam, scale=0.5, zoom=1, show_crosshair=False ):
        self.cam = cam
        self.scale = scale
        self.show_crosshair = show_crosshair
        self.zoom = zoom
        threading.Thread.__init__(self)

    @staticmethod
    def crop_and_zoom(img, zoom_factor):
        """Crop the center part of a 2d image array and repeat the pixels to match roughly the original shape."""
        h, w = img.shape
        cropx = w / zoom_factor
        cropy = h / zoom_factor
        startx = w // 2 - (cropx // 2)
        starty = h // 2 - (cropy // 2)
        return img[starty:starty + cropy, startx:startx + cropx].repeat(zoom_factor, axis=0).repeat(zoom_factor, axis=1)

    def run( self ):
        
        self.cam.cam0.start(interactive=True)
        
        if self.cam.cam0.mode.color_coding=='Y16':
            m = 255.0/65535.0           # somehow the maximum is 65532 not 65535, Guppy PROl maksimum sootuks 65523
        else:
            m = 1
        
        image = m*self.cam.cam0.current_image

        if self.zoom != 1:
            image = self.crop_and_zoom(image, self.zoom)

        if self.scale != 1:
            image = scipy.ndimage.interpolation.zoom( image, self.scale, order=1 )
        
        shape = image.shape[::-1]
        w, h = shape
        
        screen = pygame.display.set_mode(shape, 0, 8)   # greyscale
        palette = [(i,i,i) for i in xrange(256)]        # greyscale palette
        palette[-1] = (255, 0, 0)                       # max values red!
        pygame.display.set_palette( palette )
        
        pygame.surfarray.use_arraytype('numpy')
        dispimg = pygame.surfarray.pixels2d(screen)
        
        running = True
        
        while running:
            for event in pygame.event.get():
                
                if event.type == pygame.QUIT:
                    running = False
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    
            image = m*self.cam.cam0.current_image

            if self.zoom != 1:
                image = self.crop_and_zoom(image, self.zoom)
            
            if self.scale != 1:
                image = scipy.ndimage.interpolation.zoom( image, self.scale, order=1 )

            dispimg[:,:] = image.round().transpose()
            
            if self.show_crosshair:
                pygame.draw.line(screen, 0xffffff, (w/2, 0), (w/2, h), 1)
                pygame.draw.line(screen, 0xffffff, (0, h/2), (w, h/2), 1)
            
            pygame.display.update()
        
        self.cam.cam0.stop()
        pygame.quit()


class LivePlot ( threading.Thread ):
    ## ei tööta, mitte kasutada!
    def __init__ ( self, cam, axis=0 ):
        self.cam = cam
        self.axis = axis
        threading.Thread.__init__(self)
        
    def run( self ):
        
        self.cam.cam0.start(interactive=True)
        
        data = (self.cam.cam0.current_image).mean(axis=self.axis)
        
        matplotlib.pyplot.ion()
        
        fig = matplotlib.pyplot.figure()
        ax = fig.add_subplot(111)
        l, = ax.plot(data)
        ax.set_xlim(0, data.size)
        ax.set_ylim(data.min(), data.max())
        fig.canvas.draw()
        
        running = True
        
        while running:
            
            data = (self.cam.cam0.current_image).mean(axis=self.axis)
            l.set_ydata(data)
            ax.set_ylim(data.min(), data.max())
            
            fig.canvas.draw()
        
        self.cam.cam0.stop()
