#!/usr/bin/env python2
# coding=utf8

# v2013-01-31 21:38 Peeter Piksarv

# Various functions and procedures

import numpy as np
import scipy.optimize
import scipy.interpolate

from refindex import refindex
from peakdet import peakdet
import colors
   
def w2l( w, u='nm/fs' ):
    """
    Convert angular frequencies to wavelengths
    """
    
    return const_c(u) / w * 2. * np.pi
    
def l2w( l, u='nm/fs' ):
    """
    Convert wavelengths to angular frequencies
    """
    
    return const_c(u) / l * 2. * np.pi
    
def dl2dw( dl, l0, u='nm/fs' ):
    """
    Convert wavelength increment dl to angular frequency increment dw at center wavelength l0.
    """
    
    return dl * 2. * np.pi * const_c(u) / l0**2
    
def const_c( u='m/s' ):
    """
    Returns the value of the speed of light in desired units.
    Defaults 'm/s'.
    
    Accepts all SI prefixes:
        'Y' : 1e24,    # yotta
        'Z' : 1e21,    # zetta
        'E' : 1e18,    # exa
        'P' : 1e15,    # peta
        'T' : 1e12,    # tera
        'G' : 1e9,     # giga
        'M' : 1e6,     # mega
        'k' : 1e3,     # kilo
        'h' : 1e2,     # hecto
        'da': 1e1,     # deca
        'd' : 1e-1,    # deci
        'c' : 1e-2,    # centi
        'm' : 1e-3,    # milli
        'u' : 1e-6,    # micro
        'n' : 1e-9,    # nano
        'p' : 1e-12,   # pico
        'f' : 1e-15,   # femto
        'a' : 1e-18,   # atto
        'z' : 1e-21,   # zepto
        'y' : 1e-24    # yocto
        
    """
    
    c = 299792458 # http://physics.nist.gov/cgi-bin/cuu/Value?c
    
    SI = {
        'Y' : 1e24,    # yotta
        'Z' : 1e21,    # zetta
        'E' : 1e18,    # exa
        'P' : 1e15,    # peta
        'T' : 1e12,    # tera
        'G' : 1e9,     # giga
        'M' : 1e6,     # mega
        'k' : 1e3,     # kilo
        'h' : 1e2,     # hecto
        'da': 1e1,     # deca
        'd' : 1e-1,    # deci
        'c' : 1e-2,    # centi
        'm' : 1e-3,    # milli
        'u' : 1e-6,    # micro
        'n' : 1e-9,    # nano
        'p' : 1e-12,   # pico
        'f' : 1e-15,   # femto
        'a' : 1e-18,   # atto
        'z' : 1e-21,   # zepto
        'y' : 1e-24    # yocto
        }
    
    um, us = u.split('/')
    
    om = SI.get(um[:-1], 1e0)
    os = SI.get(us[:-1], 1e0)
    
    return c * os / om
    
def fwhm( x, y, axis=0 ):
    """
    Finds the full-width at half-maximum of an array on given axis. Uses linear interpolation to estimate the actual width between data points.
    
    Parameters:
    -----------
    x : array like
        Array indicies along desired axis.
    y : array like
        1D or 2D Array.
    axis : int, optional
        Axis along which to operate. By default the first axis is used.
    """
    
    if y.ndim > 1 :
        if axis == 1:
            y = y.T
            
        fw = np.zeros( y.shape[axis] )
        
        for i in np.arange( y.shape[axis] ):
            fw[i] = fwhm( x, y[i] )     # if 2D array apply this function for each row or column
    
    else:
        hm = 0.5 * y.max()              # value of half-maximum
        
        idx = np.arange(y.shape[0])  # array indicies
        
        idx_hm = idx[y > hm]
        
        i0 = idx_hm[0]
        i1 = idx_hm[-1]
        
        x0 = np.interp(hm, [y[i0-1], y[i0]], [x[i0-1], x[i0]])
        x1 = np.interp(hm, [y[i1], y[i1+1]], [x[i1], x[i1+1]])
        
        fw = x1 - x0
    
    return fw
    
def mean( x, y, axis=-1 ):
    """
    Finds the 1st raw moment of given dataset.
    
    Parameters:
    -----------
    x : array like
        Array indicies along desired axis.
    y : array like
        Data
    """
    
    mu1 = ( x * y ).sum( axis=axis ) / y.sum( axis=axis )
    
    return mu1
    
def var( x, y, m=None, axis=-1 ):
    """
    Finds the 2nd central moment of given dataset.
    
    Parameters:
    -----------
    x : array like
        Array indicies along desired axis.
    y : array like
        Data
    m :
        Mean value of data
    """
    
    if m == None:
        m = mean( x, y, axis=axis )
    
    if m.size > 1:
        mu2 = np.zeros( m.size )
        for i in xrange( m.size ):
            mu2[i] = ( (x - m[i])**2 * y[i] ).sum() / y[i].sum()
    else :
        mu2 = ( (x - m)**2 * y ).sum( axis=axis ) / y.sum( axis=axis )
    
    return mu2
    
def find_thickness( lam, phi, material='N-BK7', p0 = [0., 0., 0.], ref='air' ):
    """
    Tries to find the thickness of material by its spectral phase measurement. Varies also the 0th and 1st order of spectral phase as free parameters.
    
    Parameters:
    -----------
    lam : array like
        Wavelength calibration
    phi : array like
        Spectral phase
    material : string
        See refindex() for supported materials
    p0 : list
        Guess values for thickness, and the 0th and 1st order of spectral phase.
        Thickness in um and delay in fs.
    """
    
    omega = l2w(lam)
    
    # difference of wave vectors in medium and air in rad/um
    k = lambda w: k_n( w, material=material) - k_n( w, material=ref )
    
    # spectral phase of a p[0] thick material,
    # delay p[1] and absolute phase p[2] are free parameters
    fitfunc = lambda p, w: k( w ) * p[0] + w * p[1] + p[2]
    errfunc = lambda p, w, f: fitfunc(p, w) - f
    fit_0, success = scipy.optimize.leastsq( errfunc, p0[:], args=(omega, phi) )
    
    return fit_0
    
def k_n( omega, material='BK7' ):
    """
    Returns the wave vector in given material in rad/um.
    
    Parameters:
    -----------
    omega : array like
        angular frequency (rad/fs)
    material : string
        See refindex() for supported materials
    """
    lam = w2l(omega, u='nm/fs')
    
    return 2. * np.pi / lam * refindex( lam, material=material ) * 1000.
    
def find_max( x, y ):
    """
    Finds maximum of data using interpolation and fsolve from scipy.
    
    Parameters:
    -----------
    x : array like
        Array indicies along desired axis.
    y : array like
        Data
    """
    
    # If y is 2D, then call each row independently
    if y.ndim > 1:
        N = y.shape[0]
        xpos = np.zeros( N )
        yval = np.zeros( N )
        
        for i in xrange( N ):
            xpos[i], yval[i] = find_max( x, y[i] )
            
    else:
        # Interpolation function
        Y = scipy.interpolate.InterpolatedUnivariateSpline( x, y )
        
        # Find where 1st derivative is equal to 0
        xpos = scipy.optimize.fsolve( Y, x[y.argmax()], args=(1) )
        
        # Evaluate maximum at givven point
        yval = Y( xpos )
    
    return xpos, yval
    
def find_fwhm( x, y ):
    """
    Finds fwhm of data using interpolation and fsolve from scipy.
    
    Parameters:
    -----------
    x : array like
        Array indicies along desired axis.
    y : array like
        Data
    """
    
    # If y is 2D, then call each row independently
    if y.ndim > 1:
        N = y.shape[0]
        fw = np.zeros( N )
        
        for i in xrange( N ):
            fw[i] = find_fwhm( x, y[i] )
            
    else:
        # Interpolation function
        Y = scipy.interpolate.InterpolatedUnivariateSpline( x, y )
        
        # Find where 1st derivative is equal to 0
        xpos = scipy.optimize.fsolve( Y, x[y.argmax()], args=(1) )
        
        # Evaluate maximum at givven point
        yval = Y( xpos )
        
        # Function to solve for half maximum value
        Yh = lambda x: Y(x) - yval/2.
        
        # Using optimize.fsolve:
            # Initial guess        
            #x0 = x[y >= yval/2.]
            
            #xhm = scipy.optimize.fsolve( hY, [x0[0], x0[-1]] )
        
        # Using optimize.brentq -- should be best choice according to the scipy documentation
        # Find locations where y is more than half maximum:
        u = ( y > yval/2. )
        
        # Left edge
        i0 = u.argmax()
        xhm0 = scipy.optimize.brentq( Yh, x[i0-1], x[i0] )
        
        # Right edge index
        i1 = u[-1::-1].argmax()
        xhm1 = scipy.optimize.brentq( Yh, x[-i1-1], x[-i1] )
        
        fw = xhm1 - xhm0
    
    return fw

def inst_freq( data, dt=1. ):
    """
    
    Finds the instantaneous Frequency of given electric field. It is assumed that time axis is the last axis.
    
    Parameters:
    -----------
    data : array like
        Data
    dt : float
        the sample distance
    """
    
    data = np.angle( data )
    data = np.unwrap( data )
    data = np.gradient( data, dt )[-1]
    
    return data

RED = 646.
BLUE = 419.
    
def etx_to_rgb( etx, dt, lam ):
    global RED, BLUE
    gamma = 0.6
    
    omega = l2w( lam )
    omegao = ( omega[0] + omega[-1] ) / 2.
    
    # Absolute value
    dataa = np.abs(etx)
    dataa -= dataa.min()
    dataa /= dataa.max()
    dataa = np.power(dataa, gamma)
    
    # Find instantaneous freq.-s
    dataf = inst_freq( etx, dt ) + omegao
    dataf = w2l( dataf )
    
    # Remove wavelengths outside measurement range and rescale
    dataf -= lam.min()
    dataf /= lam.max() - lam.min()
    
    dataf[dataf<0] = 0.
    dataf[dataf>1] = 1.
    
    dataf *= RED - BLUE
    dataf += BLUE
    
    xyz = colors.l2xyz( dataf )
    colors.xyz_normalize( xyz )
    rgb = colors.xyz2rgb( xyz )
    colors.rgb_clip( rgb )
    colors.rgb_normalize( rgb )
    
    for i in xrange(3):
        rgb[...,i] *= dataa
    
    return rgb