#!/usr/bin/env python2
# coding=utf8

# v2017-10-15 15:07 Andreas Valdmann

# Class for using Thorlabs PM100 power meter

import visa
from ThorlabsPM100 import ThorlabsPM100


class PowerMeter:

    def __init__(self, device='USB0::0x1313::0x8078::P0012075::INSTR', timeout=1):
        """
        Initialize the power meter
        """

        rm = visa.ResourceManager()
        inst = rm.open_resource(resource_name=device, timeout=timeout)
        self.power_meter = ThorlabsPM100(inst=inst)

    def read(self):
        """
        Read power meter value (result in watts)
        """

        return self.power_meter.read
