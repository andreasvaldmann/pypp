#!/usr/bin/env python

# v2015-12-18 14:36 Peeter Piksarv - Update to use pyfftw
# v2012-12-19 12:11 Peeter Piksarv

import numpy.fft
import pickle
import pathlib
import pyfftw
import pyfftw.interfaces.numpy_fft as fftw

DEFAULT_WISDOM_FILE = 'C:\Users\Andreas\.config\pyfftw\pyfftw_wisdom'
NO_THREADS = 4

def check_file(wisdom_file, check_parent=True):
    '''
    Return true if file exists.
    If check_parent is set True, then also checks for the parent
    directory and creates it if required.
    .
    '''
    p = pathlib.Path(wisdom_file)
    if check_parent and not p.parent.is_dir():
        p.parent.mkdir()
        return False
    return p.is_file()

def load_wisdom( wisdom_file=DEFAULT_WISDOM_FILE ):
    '''
    Load saved FFTW wisdom from file.
    '''
    pyfftw.interfaces.cache.enable()
    
    with open(wisdom_file, 'rb') as f:
        wisdom = pickle.load( f )
        pyfftw.import_wisdom( wisdom )
        
def save_wisdom(wisdom_file=DEFAULT_WISDOM_FILE):
    '''
    Save FFTW wisdom to file.
    '''
    check_file( wisdom_file, check_parent=True )    
    with open(wisdom_file, 'wb') as f:
        pickle.dump(pyfftw.export_wisdom(), f,
                     protocol=pickle.HIGHEST_PROTOCOL)

def fft(a, n=None, axis=-1, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 1D FFT.
    
    The first three arguments are as per :func:`numpy.fft.fft`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.fft(fftw.ifftshift(a, axis), n, axis,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axis)

def fft2(a, s=None, axes=(-2, -1), overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 2D FFT.

    The first three arguments are as per :func:`numpy.fft.fft2`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.fft2(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)

def fftn(a, s=None, axes=None, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform an n-D FFT.

    The first three arguments are as per :func:`numpy.fft.fftn`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.fftn(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)   

def ifft(a, n=None, axis=-1, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 1D inverse FFT.

    The first three arguments are as per :func:`numpy.fft.ifft`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.ifft(fftw.fftshift(a, axis), n, axis,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axis)

def ifft2(a, s=None, axes=(-2, -1), overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 2D inverse FFT.

    The first three arguments are as per :func:`numpy.fft.ifft2`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.ifft2(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)

def ifftn(a, s=None, axes=None, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform an n-D inverse FFT.

    The first three arguments are as per :func:`numpy.fft.fftn`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.ifftn(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)

def rfft(a, n=None, axis=-1, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 1D real FFT.
    
    The first three arguments are as per :func:`numpy.fft.fft`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.rfft(fftw.ifftshift(a, axis), n, axis,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous)
	
def rfft2(a, s=None, axes=(-2, -1), overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 2D real FFT.

    The first three arguments are as per :func:`numpy.fft.fft2`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.rfft2(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous)

def rfftn(a, s=None, axes=None, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform an n-D real FFT.

    The first three arguments are as per :func:`numpy.fft.fftn`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.rfftn(fftw.ifftshift(a, axes), s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous)  

def irfft(a, n=None, axis=-1, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 1D real inverse FFT.

    The first three arguments are as per :func:`numpy.fft.ifft`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.irfft(a, n, axis,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axis)

def irfft2(a, s=None, axes=(-2, -1), overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform a 2D real inverse FFT.

    The first three arguments are as per :func:`numpy.fft.ifft2`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.irfft2(a, s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)

def irfftn(a, s=None, axes=None, overwrite_input=False,
    planner_effort='FFTW_ESTIMATE', threads=NO_THREADS,
    auto_align_input=True, auto_contiguous=True):
    """
    Perform an n-D real inverse FFT.

    The first three arguments are as per :func:`numpy.fft.fftn`; 
    the rest of the arguments are documented 
    in the :ref:`additional arguments docs<interfaces_additional_args>`.
    """
    return fftw.fftshift(fftw.irfftn(a, s, axes,
        overwrite_input, planner_effort, threads, auto_align_input,
        auto_contiguous), axes)