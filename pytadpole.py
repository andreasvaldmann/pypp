#!/usr/bin/env python2
# coding=utf8

# v2013-10-03 09:58
# Peeter Piksarv
# Acknowledgements: Pamela Bowlan

# Functions and procedures for SEA TADPOLE phase retrieval algorithms

import peakdet
import fftc
import numpy as np
import scipy.interpolate
import time

# Import hardware modules

# Allied Vision FireWire cameras
try:
    import camera
except ImportError:
    print('Warning pydc1394 not found. Cannot use FireWire cameras.')

# Point Grey USB3 cameras
try:
    import camera_pgr
except ImportError:
    print('Warning PyCapture2 not found. Cannot use Point Grey USB3 cameras.')

# NTS Nanodirect stage
try:
    import nanodirect
except ImportError:
    print('Warning PyNanoDrive not found. Cannot use NTS nanostage.')

# Thorlabs power meter
try:
    import powermeter
except ImportError:
    print('Warning visa not found. Cannot use power meter.')

import esp
import SLM
import flipper


from pplib import *

def find_peaks( d ):
    """Finds local maximums from a vertical slice through the Fourier transporm of the interference pattern."""
    
    peaks = peakdet.peakdet( d, 0.1)[0]
    pks = list()
    for i in range(len(peaks)):
      pks.append(peaks[i][0])
    
    return pks

def combine( a0, a1, n=10, t=65520, k=None ):
    """Combines two images of different exposures (one of the presumably overexposed and other not).
    
    Parameters:
    -----------
    a0 : 2D array
        base image
    a1 : 2D array
        merged image
    n : int, optional
        number of colums to use in the intensity scaling in the vincinity of the overexposed region
    t : value
        overexposure threshold
    k : double
        Scaling factor
    """
    
    oexp = a1.max(axis=0) > t                       # columns with overexposed pixels
    uexp = ~oexp                                    # underexposed pixels
    
    if k == None:
        idx = np.convolve(oexp, np.ones(n), 'same')     # determine the pixels that are near the overexposed region
        idx *= uexp
        idx = idx > 0
        
        k = a0[:,idx].sum()/a1[:,idx].sum()
    
    result = a0.copy()
    result[:,uexp] = a1[:,uexp]*k
    
    return result

def find_phi( meas, bg = None, pks = np.array([0, 105]), refspec = None ):
    """
    Given a 2d interference pattern meas, this program finds the phase difference by taking two 1d Fourier transforms.
    
    Returns spec, phi
    """
    
    # maksimumide asukohad üle kogu sagedusvahemiku [923, 1028, 1133]
    # kasutades Fourier pöörde sümmeetriat reaalsete algandmete puhul [0, 105]
    
    # anfft tahab single või double numbreid
    #meas = np.array(meas, dtype=np.float64)
    #bg = np.array(bg, dtype=np.float64)

    if bg is None:
        bg = np.zeros(meas.shape)
        
    sd = abs(pks[-1]-pks[-2])*2/3
    s1 = pks[-1] - sd
    s2 = pks[-1] + sd
    
    data = fftc.rfft( (meas - bg).T )
    data = fftc.ifft( data[:,s1:s2] )
    
    # Remove the first column of data
    data = data[:,1:]
    
    if refspec is None:
        refspec = bg.sum( axis=0, dtype=np.float64)
        #sisulist vahet pole, kas siin sum või mean
        
    #refspec = refspec - refspec(-1)*0.9
    spec = np.abs(data).sum( axis=1 )**2
    spec /= refspec
    
    #phi = np.unwrap(np.unwrap(np.angle(data),axis=1).mean( axis=1 ))       # kui algul unwrap üle axis=0, siis annab kaksikimpulssidega segaseid tulemusi
    phi = np.unwrap(np.angle(data[:,sd]))       # faasiinfo võtta ainult ühe rea pealt, tundub andvat parema lahutuse
    
    return spec, phi

def equally_spaced_w( omega ):
    """
    Returns an equally spaced array of angular frequencies w corresponding to a sorted array of wavelengths lam.
    """
    
    n = omega.size

    w0 = omega[0]
    w1 = omega[-1]
    
    w = np.linspace(w0, w1, n)

    #lam_0 = np.mean(lam)
    #dlam = np.mean(np.diff(lam))
    #w_0 = l2w(lam_0)
    #dw = -np.sign(dlam) * dl2dw(dlam, lam_0)
    #w = np.arange(w_0-n*dw/2., w_0+n*dw/2., dw)
    #w = (w-np.mean(w)) + w_0
    return w

def find_et( spec, phi, lam, crop = False, c1 = 0, c2 = 2452, zero_fill = True, zero_num = 2870, tlim=[-1000, 1000] ):
    """
    Given spectrum and specral phase and wavelength scale, this function returns complex electric field and time scale.
    
    Returns et, t
    """
        
    omega = l2w(lam)
    
    # Cropping and removing the constant background
    if crop == True:
        phi = phi[c1:c2]
        spec = spec[c1:c2]
        omega = omega[c1:c2]
    
    omegaeq = equally_spaced_w(omega)
    
    wo = ( omega[0] + omega[-1] ) / 2.

    omega -= wo
    omegaeq -= wo
        
    # Zero filling and interpolation
    stck = scipy.interpolate.splrep(omega,spec)
    seq = scipy.interpolate.splev(omegaeq,stck)
 
    seq[seq<0]=0	#Kui interpoleerimise tulemusena saab neg. väärtused
   
    #if seq.min() < 0:
    #    seq -= seq.min()   # Constant background removal

    ptck = scipy.interpolate.splrep(omega,phi)
    peq = scipy.interpolate.splev(omegaeq,ptck)

    
    if zero_fill == True:
        seq = np.hstack((np.zeros(zero_num), seq, np.zeros(zero_num)))
        peq = np.hstack((np.zeros(zero_num), peq, np.zeros(zero_num)))
        
    ew = np.sqrt(seq)*np.exp(-1j * peq) # Check the sign
    et = fftc.fft( ew )
    
    t = np.fft.fftshift(np.fft.fftfreq(ew.shape[0], np.diff(omegaeq).mean()/(2.*np.pi)))
    
    # Limit the temporal range
    if tlim != None :
        i0 = (t>tlim[0]).argmax()
        i1 = (t>tlim[1]).argmax()
        
        t = t[i0:i1]
        et = et[i0:i1]
    
    return et, t

def find_etx( spec, phi, lam, crop = False, c1 = 0, c2 = 2452, zero_fill = True, zero_num = 2870, tlim=[-1000, 1000]  ):
    """
    Given spectrum and specral phase and wavelength scale, this function returns complex electric field and time scale.
    Works simultaneously on entire scan.
    
    Returns et, t
    """
    
    nox = spec.shape[0]
        
    omega = l2w(lam)
    
    # Cropping and removing the constant background
    if crop == True:
        phi = phi[:,c1:c2]
        spec = spec[:,c1:c2]
        omega = omega[c1:c2]
    
    omegaeq = equally_spaced_w(omega)
    
    wo = ( omega[0] + omega[-1] ) / 2.

    omega -= wo
    omegaeq -= wo
    
    seq = np.zeros(spec.shape)
    peq = np.zeros(spec.shape)
    
    # Zero filling and interpolation
    for i in np.arange(nox):
        stck = scipy.interpolate.splrep(omega,spec[i,:])
        seq[i,:] = scipy.interpolate.splev(omegaeq,stck)
        #seq[i,:] -= seq[i,:].min()     # Constant background removal
        
        ptck = scipy.interpolate.splrep(omega,phi[i,:])
        peq[i,:] = scipy.interpolate.splev(omegaeq,ptck)
    
    seq[seq<0]=0	#Kui interpoleerimise tulemusena saab neg. väärtused

    if zero_fill == True:
        seq = np.hstack((np.zeros((nox, zero_num)), seq, np.zeros((nox, zero_num))))
        peq = np.hstack((np.zeros((nox, zero_num)), peq, np.zeros((nox, zero_num))))
        
    ew = np.sqrt(seq)*np.exp(-1j * peq) # Check the sign
    et = fftc.fft( ew )
    
    t = np.fft.fftshift(np.fft.fftfreq(ew.shape[-1], np.mean(np.diff(omegaeq))/(2.*np.pi)))
    
    # Limit the temporal range
    if tlim != None :
        i0 = (t>tlim[0]).argmax()
        i1 = (t>tlim[1]).argmax()
        
        t = t[i0:i1]
        et = et[:,i0:i1]
    
    return et, t
  
def find_etxy( spec, phi, lam, crop = False, c1 = 0, c2 = 2452, zero_fill = True, zero_num = 2870, tlim=[-1000, 1000]  ):
    """
    Given spectrum and specral phase and wavelength scale, this function returns complex electric field and time scale.
    Works simultaneously on entire x,y-scan.
    
    Returns et, t
    """
    
    nox = spec.shape[0]
    noy = spec.shape[1]
    nop = nox*noy
    
    row, col = np.indices(spec.shape[0:2])
    row = row.flatten()
    col = col.flatten()
        
    omega = l2w(lam)
    
    # Cropping and removing the constant background
    if crop == True:
        phi = phi[:,:,c1:c2]
        spec = spec[:,:,c1:c2]
        omega = omega[c1:c2]
    
    omegaeq = equally_spaced_w(omega)
    
    wo = ( omega[0] + omega[-1] ) / 2.

    omega -= wo
    omegaeq -= wo
    
    seq = np.zeros(spec.shape)
    peq = np.zeros(spec.shape)
    
    # Zero filling and interpolation
    for i in np.arange(nop):
        stck = scipy.interpolate.splrep(omega,spec[row[i],col[i],:])
        seq[row[i],col[i],:] = scipy.interpolate.splev(omegaeq,stck)
        #seq[i,:] -= seq[i,:].min()     # Constant background removal
        
        ptck = scipy.interpolate.splrep(omega,phi[row[i],col[i],:])
        peq[row[i],col[i],:] = scipy.interpolate.splev(omegaeq,ptck)
    
    seq[seq<0]=0	#Kui interpoleerimise tulemusena saab neg. väärtused

    if zero_fill == True:
        seq = np.dstack((np.zeros((nox, noy, zero_num)), seq, np.zeros((nox, noy, zero_num))))
        peq = np.dstack((np.zeros((nox, noy, zero_num)), peq, np.zeros((nox, noy, zero_num))))
        
    ew = np.sqrt(seq)*np.exp(-1j * peq) # Check the sign
    et = fftc.fft( ew )
    
    t = np.fft.fftshift(np.fft.fftfreq(ew.shape[-1], np.mean(np.diff(omegaeq))/(2.*np.pi)))
    
    # Limit the temporal range
    if tlim != None :
        i0 = (t>tlim[0]).argmax()
        i1 = (t>tlim[1]).argmax()
        
        t = t[i0:i1]
        et = et[:,:,i0:i1]
    
    return et, t


def init_cam( n = 0 ):
    """Initializes camera, check camera number!"""
    
    return camera.Camera(n)


def init_pgr_cam(n=0):
    """Initializes Point Grey camera, check camera number!"""

    return camera_pgr.Camera(n)


def init_stage( port = 'COM3', axes = '1' ):
    """Initializes stage on ESP301 controller"""
    if len(axes)==1:
        return esp.Esp( port = port, axis = axes )
    else:
        stages = []
        ser = None
        for axis in axes:
            if ser is None:
                stage = esp.Esp( port = port, axis = axis )
                stages.append(stage)
                ser = stage.serial
            else:
                stage = esp.Esp( port = port, axis = axis, ser = ser )
                stages.append(stage)
        return stages

def init_nanodirect():
    """Initializes stage on NTS nanodirect controller"""

    return nanodirect.Nanodirect()

def init_slm( size = (1024, 768), wpos = (0, 0) ):
    """Initializes SLM device"""
    
    return SLM.SLM( size, wpos )

def init_flipper( port = 'COM3',  devs = 'a'):
    """Initializes flipper"""
    if len(devs)==1:
        return flipper.Flipper( port = port, dev = devs )
    else:
        flippers = []
        ser = None
        for dev in devs:
            if ser is None:
                flip = flipper.Flipper( port = port, dev = dev )
                flippers.append(flip)
                ser = flip.serial
            else:
                flip = flipper.Flipper( port = port, dev = dev, ser = ser )
                flippers.append(flip)
        return flippers


def init_power_meter(device='USB0::0x1313::0x8078::P0012075::INSTR', timeout=1):
    """Initializes Thorlabs PM100 power meter"""

    return powermeter.PowerMeter(device=device, timeout=timeout)
    

def scan_x( start, stop, step, bg, stage, cam, refspec = None, pks = np.array([0, 105]), home = True, lead = 0.015):
    """
    Measures the spectrum and phase at each x along the crossection of a beam.
    start, stop -- starting and ending position of scan (mm) (stop included)
    step -- step size (mm)
    bg -- background measurement
    stage -- stage object
    cam -- camera object
    pks -- peaks of Fourier transform of SEA TADPOLE trace
    home -- if True, returns to initial position
    lead -- distance that the stage moves before the first measurement point (mm)
    
    Returns x, spec, phi
    """
    
    pos = stage.getPos()
    
    x = np.arange(start, stop+step, step)
    
    # initialize measurement matrices
    spec = np.zeros((x.size, bg.shape[-1]))
    phi = np.zeros((x.size, bg.shape[-1]))
    
    stage.goTo(start - lead)
    stage.wait()  
    
    for i in np.arange(x.size):
        stage.goTo( x[i] )
        stage.wait()
        dat = cam.getFrame()
        spec[i,:], phi[i,:] = find_phi(dat, bg, pks, refspec)
    
    if home == True:
        stage.goTo(pos)
    
    return x, spec, phi

def scan_xy( start, stop, step, bg, stage_x, stage_y, cam, refspec = None, pks = np.array([0, 105]), home = True, lead = 0.015 ):
    """
    Measures the spectrum and phase at each x & y along the crossection of a beam.
    start, stop -- starting and ending position of scan (mm) (stop included)
    step -- step size (mm)
    bg -- background measurement
    stage -- stage object
    cam -- camera object
    pks -- peaks of Fourier transform of SEA TADPOLE trace
    home -- if True, returns to initial position
    lead -- distance that the stage moves before the first measurement point (mm)
    
    Returns x, spec, phi
    """
    
    pos_x = stage_x.getPos()
    pos_y = stage_y.getPos()
    
    x = np.arange(start, stop+step, step)
    
    # initialize measurement matrices
    spec = np.zeros((x.size, x.size, bg.shape[-1]))
    phi = np.zeros((x.size, x.size, bg.shape[-1]))
    
    stage_y.goTo(start - lead)
    stage_y.wait()
    
    for i in np.arange(x.size):
        stage_y.goTo( x[i] )
        stage_y.wait()
        
        stage_x.goTo(start - lead)
        stage_x.wait()
        
        for j in np.arange(x.size):
            stage_x.goTo( x[j] )
            stage_x.wait()
            dat = cam.getFrame()
            spec[i,j,:], phi[i,j,:] = find_phi(dat, bg, pks, refspec)
    
    if home == True:
        stage_x.goTo(pos_x)
        stage_y.goTo(pos_y)
    
    return x, spec, phi

def scan_xy2( xstart, xstop, xstep, ystart, ystop, ystep, bg, stage_x, stage_y, cam, refspec = None, pks = np.array([0, 105]), home = True, lead = 0.015 ):
    """
    Measures the spectrum and phase at each x & y along the crossection of a beam.
    xstart (ystart), xstop (ystop) -- starting and ending position of scan (mm) (stop included)
    xstep (ystep) -- step size (mm)
    bg -- background measurement
    stage -- stage object
    cam -- camera object
    pks -- peaks of Fourier transform of SEA TADPOLE trace
    home -- if True, returns to initial position
    lead -- distance that the stage moves before the first measurement point (mm)

    Returns x, y, spec, phi
    """

    pos_x = stage_x.getPos()
    pos_y = stage_y.getPos()

    x = np.arange(xstart, xstop+xstep, xstep)
    y = np.arange(ystart, ystop+ystep, ystep)

    # initialize measurement matrices
    spec = np.zeros((y.size, x.size, bg.shape[-1]))
    phi = np.zeros((y.size, x.size, bg.shape[-1]))

    stage_y.goTo(ystart - lead)
    stage_y.wait()

    for i in np.arange(y.size):
        stage_y.goTo( y[i] )
        stage_y.wait()

        stage_x.goTo(xstart - lead)
        stage_x.wait()

        for j in np.arange(x.size):
            stage_x.goTo( x[j] )
            stage_x.wait()
            dat = cam.getFrame()
            spec[i,j,:], phi[i,j,:] = find_phi(dat, bg, pks, refspec)

    if home == True:
        stage_x.goTo(pos_x)
        stage_y.goTo(pos_y)

    return x, y, spec, phi
    
def scan_xyI( xstart, xstop, xstep, ystart, ystop, ystep, stage_x, stage_y, cam, home = True, lead = 0.015 ):
    """
    Measures the spectrum and phase at each x & y along the crossection of a beam.
    xstart (ystart), xstop (ystop) -- starting and ending position of scan (mm) (stop included)
    xstep (ystep) -- step size (mm)
    bg -- background measurement
    stage -- stage object
    cam -- camera object
    pks -- peaks of Fourier transform of SEA TADPOLE trace
    home -- if True, returns to initial position
    lead -- distance that the stage moves before the first measurement point (mm)
    
    Returns x, y, spec, maximum
    """

    pos_x = stage_x.getPos()
    pos_y = stage_y.getPos()

    x = np.arange(xstart, xstop+xstep, xstep)
    y = np.arange(ystart, ystop+ystep, ystep)

    # initialize measurement matrices
    spec = np.zeros((y.size, x.size, 2452))
    maximum = np.zeros((y.size, x.size))

    stage_y.goTo(ystart - lead)
    stage_y.wait()

    for i in np.arange(y.size):
        stage_y.goTo( y[i] )
        stage_y.wait()

        stage_x.goTo(xstart - lead)
        stage_x.wait()

        for j in np.arange(x.size):
            stage_x.goTo( x[j] )
            stage_x.wait()
            dat = cam.getFrame()
            spec[i,j,:] = dat.mean(axis=0)
            maximum[i,j] = dat.max()

    if home == True:
        stage_x.goTo(pos_x)
        stage_y.goTo(pos_y)

    return x, y, spec, maximum

def find_peak_coords(xstart, xstop, xstep, ystart, ystop, ystep, stage_x, stage_y, cam, home = True ):
    """
    Measures xy intensity distribution and finds coordinates of the peak.
    xstart (ystart), xstop (ystop) -- starting and ending position of scan (um) (stop included)
    xstep (ystep) -- step size (um)
    stage -- stage object
    cam -- camera object
    home -- if True, returns to initial position

    Returns x_max, y_max
    """
    
    pos_x = stage_x.getPos()
    pos_y = stage_y.getPos()

    x = np.arange(xstart, xstop+xstep, xstep)
    y = np.arange(ystart, ystop+ystep, ystep)

    # initialize measurement matrix
    dat = np.zeros((y.size, x.size))

    stage_y.goTo(ystart - ystep)
    stage_y.wait()

    for i in np.arange(y.size):
        stage_y.goTo( y[i] )
        stage_y.wait()

        stage_x.goTo(xstart - xstep)
        stage_x.wait()

        for j in np.arange(x.size):
            stage_x.goTo( x[j] )
            stage_x.wait()
            dat[i,j] = cam.getFrame().mean()
            
    if home == True:
        stage_x.goTo(pos_x)
        stage_y.goTo(pos_y)            
     
    index_y_max, index_x_max = np.unravel_index(dat.argmax(), dat.shape)
    
    x_max = x[index_x_max]
    y_max = y[index_y_max]
     
    return x_max, y_max
    
def cal_lam( meas, bg = None, pks = np.array([0, 105]), theta = 0.005, lam0 = None, px = 3.45 ):
    """
    Calibrates wavelength axis using fringe spacement on SEA TADPOLE trace.
    
    Parameters:
    -----------
    meas : array like
        SEA TADPOLE trace
    bg : array like
        Background image
    pks : array like
        Peak positions on the Fourier of measurement data.
    theta : float
        1/2 of crossing angle between reference and measurement beam.
    lam0 : [index, wavelength]
        If given, it is used to calculate theta.
    px : float
        Camera pixel size in microns.
    """
    
    if bg is None:
        bg = np.zeros(meas.shape)
        
    sd = abs(pks[-1]-pks[-2])*2/3
    s1 = pks[-1] - sd
    s2 = pks[-1] + sd
    
    data = fftc.rfft( (meas - bg).T )
    kc = np.fft.fftfreq(meas.shape[0], px/2./np.pi)    # Camera coordinates in Fourier space
    
    cut = abs(data[:, s1:s2])
    kcut = kc[s1:s2]
    
    im = cut.argmax(axis=-1)                      # Positions of side maximums
    q = np.arange(im.size)
    
    km = scipy.interpolate.UnivariateSpline(q, kcut[im], k=5)(q)        # Smoothed positions of maximums
    
    if lam0 != None:
        theta = km[lam0[0]]*const_c('um/fs') / (2. * l2w(lam0[1]))
        
    omega = km * const_c('um/fs') / (2. * theta)
    lam = w2l(omega)    
    
    return lam

def get_cal_lam( cam, shutter = np.array([4000, 2000, 1000]), pks = np.array([0, 105]), theta = 0.005, lam0 = None, px = 3.45 ):
    """
    Calibrates wavelenght axis using fringe spacement on SEA TADPOLE trace. 
    Uses subsequent camera images with different shutter speeds to compose
    a high dynamic range image.
    
    Parameters:
    cam : camera object
    
    shutter: array like
        Desired shutter speeds (from slowest to fastest)
    pks : array like
        Peak positions on the Fourier of measurement data.
    theta : float
        1/2 of crossing angle between reference and measurement beam.
    lam0 : [index, wavelenght]
        If given, it is used to calculate theta.
    px : float
        Camera pixel size in microns.
    """
    
    data = np.ones( cam.cam0.mode.shape )*65535.0        # Camera image size
    shutter = np.sort(shutter)[::-1]
    
    for i in xrange( shutter.size ):
        cam.setShutter( shutter[i] )
        time.sleep(0.5)
        frame = cam.getFrame()
        
        d1 = ((data>65520).sum(axis=0)>0).argmax()
        d2 = data.shape[1]-((data>65520).sum(axis=0)>0)[-1::-1].argmax()
        
        data[:,d1:d2] = frame[:,d1:d2]
        
    lam = cal_lam( data, bg = None, pks = pks, theta = theta, lam0 = lam0, px = px )
    
    return lam

def get_bg( cam, shutter = np.array([4000, 2000, 1000]), n = 20 ):
    """
    Get background image
    
    Parameters:
    cam : camera object
    
    shutter: array like
        Desired shutter speeds (from slowest to fastest)
    n : int
        number of repetitions
    """
    
    
    data = np.zeros( cam.cam0.mode.shape )         # Camera image size
    
    for i in xrange( n ):
        for j in xrange( shutter.size ):
            cam.setShutter( shutter[-1-j] )
            
            if j == 0:
                datai = cam.getFrame()
                k0 = 38. + cam.getExtdShutter()      #38. for Stingray F504B shutter offset
            else:
                datai = combine( datai, cam.getFrame(), k = k0 / (38. + cam.getExtdShutter()) )
                
        data += datai / float( n )
    
    return data

def get_phi( cam, shutter = np.array([4000, 2000, 1000]), bg = None, pks = np.array([0, 105]), refspec = None ):
    """
    Retrieves the phase difference by taking two 1d Fourier transforms from spatial spectral interferometry trace.
    HDR version.
    
    Returns spec, phi
    """
    
    for i in xrange( shutter.size ):
        cam.setShutter( shutter[-1-i] )
        
        if i == 0:
            meas = cam.getFrame()
            k0 = 38. + cam.getExtdShutter()      #38. for Stingray F504B shutter offset
        else:
            meas = combine( meas, cam.getFrame(), k = k0 / (38. + cam.getExtdShutter()) )
            
    spec, phi = find_phi( meas, bg = bg, pks = pks, refspec = refspec ) 
    
    return spec, phi

def scan_xy_slm2( start, stop, step, bg, stage_x, stage_y, cam, slm, images, refspec = None, pks = np.array([0, 105]), home = True, lead = 0.015):
    """
    Scan_xy version with SLM control.
    Measures the spectrum and phase at each x & y along the crossection of a beam.
    start, stop -- starting and ending position of scan (um) (stop included)
    step -- step size (um)
    bg -- background measurement
    stage -- stage object
    cam -- camera object
    slm -- slm object
    image -- list on arrays of image put on the slm
    pks -- peaks of Fourier transform of SEA TADPOLE trace
    home -- if True, returns to initial position
    lead -- distance that the stage moves before the first measurement point (mm)
    
    Returns x, spec, phi
    """
    
    pos_x = stage_x.getPos()
    pos_y = stage_y.getPos()
    cur_image = slm.params['image']
    
    x = np.arange(start, stop+step, step)
    
    # initialize measurement matrices
    N = images.__len__()
    spec = []
    phi = []
    for i in np.arange(N):
        spec.append( np.zeros((x.size, x.size, bg.shape[-1])) )
        phi.append( np.zeros((x.size, x.size, bg.shape[-1])) )
    
    stage_y.goTo(start - lead)
    stage_y.wait()
    
    # Reset image on SLM
    slm.setImage( images[0] )
    t1 = time.time()
    
    for i in np.arange(x.size):
        stage_y.goTo( x[i] )
        stage_y.wait()
        
        stage_x.goTo(start - lead)
        stage_x.wait()
        
        for j in np.arange(x.size):
            stage_x.goTo( x[j] )
            stage_x.wait()
            
            for k in np.arange(N):
                #1: Find spectrum and phase with images[k] (should be on SLM)
                t2 = time.time()
                dt = t2 - t1
                if dt < 0.25:
                    time.sleep( 0.25 - dt )
                dat = cam.getFrame()
                slm.setImage( images[ np.mod(k+1,N) ] )       # Set image to the measurement, use the time delay of find_phi() for waiting it to take effect
                t1 = time.time()
                spec[k][i,j,:], phi[k][i,j,:] = find_phi( dat, bg, pks, refspec )
    
    if home == True:
        stage_x.goTo( pos_x )
        stage_y.goTo( pos_y )
        slm.setImage( cur_image )
    
    return x, spec, phi
