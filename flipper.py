#!/usr/bin/env python2
# coding=utf8

# v2013-09-20 12:09 Peeter Piksarv

# Class for using Newport Motorized Flipper Mirror holder
# Special electronics COM->TTL by Agu Anijalg

import serial
import time


class Flipper:
  
    def __init__( self,  dev='a', port = 'COM3', ser=None ):
        """
        Initialize the flipper
        """
        
        if ser is None:
            self.serial = serial.Serial(port=port,
                                        baudrate=9600)
        else:
            self.serial = ser
        
        assert dev in ('a', 'b', 'c', 'kl')
        self.dev = dev
        
        #self.serial.open()        
        
    def up(self):
        """ Move flipper to up position """
        self.serial.write('{0}_up,'.format(self.dev))
        
    def down(self):
        """ Move flipper to down position """
        self.serial.write('{0}_down,'.format(self.dev))
        
    def singleShot(self, duration):
        """ Move flipper to down position for the given duration and return to up position """

        self.down()
        time.sleep(duration)
        self.up()


# There's no point in checking the current position, as it can at any moment
# manually overridden. Only way to make sure, is to move the flipper to 
# the desired position.
