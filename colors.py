#!/usr/bin/env python2
# coding=utf8

# v2013-02-01 21:50 Peeter Piksarv

# Colorspace and color maching functions
# Inspired by ColorPy package by Mark Kness http://markkness.net/colorpy/ColorPy.html

import numpy as np
from scipy import interpolate

import colors_data

def l2xyz( l, formulary='CIEXYZ_2012' ):
    '''Given a wavelength in nm, returns the corresponding XYZ color for unit intensity.'''
    
    TableXYZ = colors_data.colorMatchFn( formulary )
    
    MatchFn = interpolate.interp1d(TableXYZ[:,0], TableXYZ[:,1:], axis=0, fill_value=0, bounds_error=False)
    
    return MatchFn( l )

def xyz_normalize( xyz ):
    '''Scale so that all values add to 1.0.
    This both modifies the passed argument and returns the normalized result.'''
    xyz_sum = xyz.sum(axis=-1)
    
    if len(xyz.shape) == 1:
        if xyz_sum != 0.0:
            xyz /= xyz_sum
    else:
        idx = xyz_sum != 0.0
    
        for i in xrange(3):
            xyz[idx,i] /= xyz_sum[idx]
        
    return xyz

def xyz2rgb( xyz ):
    '''Convert an xyz color to rgb.'''
    
    # sRGB, from http://www.color.org/sRGB.xalter
    #xyz2srgb_matrix = np.array ([
    #    [ 3.2410, -1.5374, -0.4986],
    #    [-0.9692,  1.8760,  0.0416],
    #    [ 0.0556, -0.2040,  1.0570]
    #])
    # http://web.archive.org/web/20030212204955/www.srgb.com/basicsofsrgb.htm, http://www.color.org/sRGB.pdf
    xyz2srgb_matrix = np.array ([
        [ 3.2406, -1.5372, -0.4986],
        [-0.9689,  1.8758,  0.0415],
        [ 0.0557, -0.2040,  1.0570]])
    xyz_shape = xyz.shape
    
    xyz = xyz.reshape((xyz.size/3, 3))
    rgb = np.zeros( xyz.shape )
    for i in xrange( xyz.size/3 ):
        rgb[i] = np.dot(xyz2srgb_matrix, xyz[i])
    rgb = rgb.reshape( xyz_shape )
    
    return rgb

def rgb_clip( rgb ):
    '''Convert a linear rgb color (nominal range 0.0 - 1.0), into a displayable
    rgb color with values in the nominal range, clipping as necessary.'''
    
    #add enough white to make all rgb values nonnegative
    # find max negative rgb (or 0.0 if all non-negative), we need that much white
    
    rgb_min = rgb.min(axis=-1)
    rgb_max = rgb.max(axis=-1)
    
    if len(rgb.shape) == 1:
        if rgb_min < 0.0:
            rgb -= rgb_min
            if rgb_max > 0.0:
                rgb *= rgb_max / (rgb_max-rgb_min)
    else:
        idx = rgb_min < 0.0
        idx_ = idx * (rgb_max > 0.0)
        
        for i in xrange(3):
            rgb[idx, i] -= rgb_min[idx]
            rgb[idx_, i] *= rgb_max[idx_] / (rgb_max[idx_] - rgb_min[idx_])
    
    # clip intensity if needed (rgb values > 1.0) by scaling
    rgb_max = rgb.max(axis=-1)
    
    if len(rgb.shape) == 1:
        if rgb_max > 1.0:
            rgb /= rgb_max
    else:
        idx = rgb_max > 1.0
        
        for i in xrange(3):
            rgb[idx, i] /= rgb_max[idx]
    
    srgb_gamma_invert( rgb )
    
    return rgb
    
def srgb_gamma_invert( x ):
    '''sRGB standard for gamma inverse correction.
    This both modifies the passed argument and returns the corrected result.'''
    idx = x<= 0.00304
    idx_ = np.invert( idx )
    x[ idx ] *= 12.92
    x[ idx_ ] = 1.055 * np.power (x[ idx_ ], 1.0/2.4) - 0.055
    
    return x

def rgb_normalize( rgb ):
    '''Scale each color to have the max rgb component equal to the desired brightness.
    This both modifies the passed argument and returns the normalized result.'''
    rgb_max = rgb.max(axis=-1)
    if len(rgb.shape) == 1:
        if rgb_max > 0:
            rgb /= rgb_max
    else:
        idx = rgb_max > 0
        for i in xrange(3):
            rgb[idx, i] /= rgb_max[idx]
        
    return rgb