#!/usr/bin/env python2
# coding=utf8

# v2017-10-14 23:34 Andreas Valdmann

# Class for using NTS nanopositioning stage
# Uses PyNanoDrive wrapper by Ardi Loot

import PyNanoDrive


class Nanodirect:

    def __init__(self):
        """
        Initialize the nanodirect controller and motor
        """

        self.controller = PyNanoDrive.NanoDriveController()
        self.motor = self.controller.motors[0]
        self.controller.Connect()

        self.motor.SetMode('Continuous')
        self.motor.SetUnit('mm')

    def zeroStage(self):
        """Find stage zero position"""

        self.motor.ZeroStage()

    def goTo(self, pos):
        """Go to absolute position"""

        self.motor.MoveToAbsolutePosition(pos)

    def disconnect(self):
        """Disconnect controller"""

        self.controller.Disconnect()
