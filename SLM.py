#!/usr/bin/env python2
# coding=utf8

# v2012-01-02 21:36 Peeter Piksarv

# Class for using controlling SLM

import pygame
import os
import numpy
import scipy.interpolate

from PIL import Image

import multiprocessing

class SLM:
  
  def __init__( self, size = (1024, 768), wpos = (0, 0) ):
    """
    Init SLM class
    
    Parameters:
    size : tup
        SLM image size
    pos : tup
        SLM window position
    """
    
    self.manager = multiprocessing.Manager()
    self.params = self.manager.dict()
    
    self.params['size'] = size
    self.params['wpos'] = wpos
    
    self.params['image'] = numpy.zeros( size, dtype=numpy.uint8 )
    
  def show( self, image = None, pos = (0, 0) ):
    """
    Starts SLM Window
    
    Parameters:
    image : string or array like
        Path to an image file or array of size
    pos : tup
        SLM image position shift
    """
    self.setImage( image )
    self.setPos( pos )
    
    slm = multiprocessing.Process( target = self.SLMWindow, args=( self.params, ) )
    slm.start()
    
  def showRandom( self, t = 0, pos = (0, 0) ):
    """
    Starts SLM Window with random noise
    
    Parameters:
    t : double
        time delay between random images in milliseconds
    pos : tup
        SLM image position shift
    """
    self.setPos( pos )
    
    slm = multiprocessing.Process( target = self.SLMWindowRandom, args=( self.params, t, ) )
    slm.start()
    
  def setImage( self, image = None ):
      """
      Trys to set SLM image to image.
      
      Parameters:
      image : string or array like
          Path to an image file or array of size
      """
      
      try:
          I = numpy.asarray( Image.open(image), dtype=numpy.uint8 )
          if I.ndim == 3:
              I = I[:,:,0]
          self.params['image'] = I.T
          
      except:
          try:
              if image.shape == self.params['image'].shape:
                self.params['image'] = image
          except:
              self.params['image'] = gen_box()
  def setPos( self, pos = (0,0) ):
      """
      Sets SLM image position to pos.
      
      Parameters:
      pos: tup
        SLM image position shift
      """
      
      self.params['pos'] = pos
      
  def SLMWindow( self, params ):
    """
    Shows SLM image window, assumes that SLM screen is at top left corner.
    """
    
    pygame.init()
    
    black = (0, 0, 0)                       # Black colour
    palette = [(i,i,i) for i in xrange(256)]    # Pallete for grayscale screen
    
    os.environ['SDL_VIDEO_WINDOW_POS'] = str(params['wpos'][0]) + "," + str(params['wpos'][1])
    screen = pygame.display.set_mode( params['size'], pygame.NOFRAME, 8 )   # Grayscale screen
    pygame.display.set_palette( palette )
    
    os.environ['SDL_VIDEO_WINDOW_POS'] = ''	# Unset env. variable for other windows
    
    pygame.display.set_caption( 'SLM' )
    pygame.surfarray.use_arraytype( 'numpy' )
    pygame.key.set_repeat( 660, 40 )
        
    running = True
    while running:
    
      for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
          if event.key == pygame.K_ESCAPE:  # Exit when Esc is pressed
            running = False
            
          elif event.key == pygame.K_RIGHT: # Arrow keys to move image around
            params['pos'] = ( params['pos'][0] + 1, params['pos'][1] )
            screen.fill( black )
          elif event.key == pygame.K_LEFT:
            params['pos'] = ( params['pos'][0] - 1, params['pos'][1] )
            screen.fill( black )
          elif event.key == pygame.K_UP:
            params['pos'] = ( params['pos'][0], params['pos'][1] - 1 )
            screen.fill( black )
          elif event.key == pygame.K_DOWN:
            params['pos'] = ( params['pos'][0], params['pos'][1] + 1 )
            screen.fill( black )
            
          elif pygame.K_0 <= event.key and event.key <= pygame.K_9:   # Number keys to fill image with gray color
            params['image'] = gen_uniform( (event.key - pygame.K_0)/9. )
            
          elif event.key == pygame.K_x:     # X to reset image to default
            params['image'] = gen_box()
      
      pygame.surfarray.blit_array( screen, params['image'] )
      pygame.display.update()
      #image = array_to_image( params['image'] )
      #image.set_palette( palette )
      #screen.blit( image, params['pos'] )
      #pygame.display.flip()
      
      pygame.time.wait( 10 )               # Delay the loop
      
    pygame.quit()
    
        
  def SLMWindowRandom( self, params, t = 0 ):
    """
    Shows SLM image window, assumes that SLM screen is at top left corner.
    """
    
    size = params['size']
    
    pygame.init()
    
    os.environ['SDL_VIDEO_WINDOW_POS'] = str(params['wpos'][0]) + "," + str(params['wpos'][1])
    
    screen = pygame.display.set_mode( size, pygame.NOFRAME, 8 )     # Grayscale screen
    pygame.display.set_palette( [(i,i,i) for i in xrange(256)] )    # Pallete for grayscale screen
    
    os.environ['SDL_VIDEO_WINDOW_POS'] = '' # Unset env. variable for other windows
    
    pygame.display.set_caption( 'SLM' )
    pygame.surfarray.use_arraytype( 'numpy' )
    dispimg = pygame.surfarray.pixels2d( screen )
        
    running = True
    while running:
    
      for event in pygame.event.get():
        
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
      
      image = numpy.random.random_integers( 0, 255, size )
      
      pygame.surfarray.blit_array( screen, image )
      
      pygame.display.update()
      
      if t > 0:
          pygame.time.wait( t )               # Delay the loop
      
    pygame.quit()
       
def array_to_image( a, size = (1024, 768) ):
  # Converts an array to an image.
  a = a.astype(numpy.uint8)
  image = pygame.image.frombuffer( a.T.tostring( ), size, 'P' )
  
  return image

## SLM mask generators: ##

def gen_uniform( value = 0., size = (1024, 768) ):
  # Generates uniform pattern.
  
  a = numpy.ones( size )
  a *= value
  
  return (a * 255).astype( numpy.uint8 )

def gen_box( black = 0., white = 1., size = (1024, 768) ):
  # Generates checkerboard pattern dividing screen to 4 equal parts.
  
  a = numpy.zeros( size )
  a += black
  
  # Set the lower left quater and the upper right quater of the image to white
  a[:size[0]/2, :size[1]/2] = white
  a[size[0]/2:, size[1]/2:] = white
      
  return (a * 255).astype( numpy.uint8 )

def gen_gradient( size = (1024, 768) ):
  # Generates horisontal gray gradient from black to white
  
  gradient = numpy.linspace(0, 255, size[0])
  a = numpy.ones( size ).T
  a *= gradient
  
  return numpy.around(a.T).astype( numpy.uint8 )

def gen_diffgr( d = 1, h = 255, size = (1024, 768) ):
    # Generates diffraction grating pattern with period 2d pixels and grating depth of h.
    
    grating = numpy.zeros( size[0] )
    for i in range(d):
        grating[d+i::2*d] = h
        
    a = numpy.ones( size ).T
    a *= grating
    
    return (a.T).astype( numpy.uint8 )

def gen_fork(n=1, lines=256, size = (1024, 768)):
    
    X, Y = numpy.meshgrid(numpy.arange(size[0]), numpy.arange(size[1]))
    lin_phase = 2*numpy.pi*lines*X/X.max()
    X -= X.mean()
    Y -= Y.mean()
    spiral_phase = n*numpy.angle(1j*X + Y)
    
    result = 186*((lin_phase + spiral_phase) % (2*numpy.pi))/(2*numpy.pi) 
   
    return (result.T).astype('uint64')

def rad_to_gray( a, cal ):
    """
    Converts phase map given in radians to corresponding gray values using
    calibration data.
    
    Parameters:
    a : array like
        Phase map in radians
    cal : array like
        Curve of SLM's phase response, for each gray value corresponding phase modulation
    """
    
    phase_max = cal.max()
    
    phase_func = scipy.interpolate.InterpolatedUnivariateSpline( cal, numpy.arange( cal.size ) )
    
    a_wrapped = numpy.mod( a, phase_max )
    
    image = phase_func( a_wrapped.flatten() )
    image = image.reshape( a.shape )
    image = image.round()
    
    return image.astype( numpy.uint8 )
    
    
    